<?php

use Faker\Generator as Faker;
use App\Country as Model;

$factory->define(Model::class, function (Faker $faker) {
    return [
        'name' => $faker->country,
    ];
});
