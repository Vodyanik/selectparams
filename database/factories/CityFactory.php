<?php

use Faker\Generator as Faker;
use App\City as Model;

$factory->define(Model::class, function (Faker $faker) {
    return [
        'region_id' => App\Region::pluck('id')->random(),
        'name' => $faker->city,
    ];
});
