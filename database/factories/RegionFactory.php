<?php

use Faker\Generator as Faker;
use App\Region as Model;

$factory->define(Model::class, function (Faker $faker) {
    return [
        'country_id' => App\Country::pluck('id')->random(),
        'name' => 'region' . $faker->country,
    ];
});
