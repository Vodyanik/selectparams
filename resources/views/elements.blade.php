<div class="col-md-4">
    <select id="select-country" class="form-control">
        @if(empty($countries))
            <option value="0" disabled selected>Countries is empty</option>
        @elseif(empty($country_id))
            <option value="0" disabled selected>Please select the country</option>
        @endif
        @foreach($countries as $country)
            @if($country->id == $country_id)
                <option value="{{ $country->id }}" selected>{{ $country->name }}</option>
            @else
                <option value="{{ $country->id }}">{{ $country->name }}</option>
            @endif
        @endforeach
    </select>
</div>
<div class="col-md-4">
    <select id="select-region" class="form-control">
        @if(empty($regions))
            <option value="0" disabled selected>Regions is empty</option>
        @elseif(empty($region_id))
            <option value="0" disabled selected>Please select the country</option>
        @endif
        @foreach($regions as $region)
            @if($region->id == $region_id)
                <option value="{{ $region->id }}" selected>{{ $region->name }}</option>
            @else
                <option value="{{ $region->id }}">{{ $region->name }}</option>
            @endif
        @endforeach
    </select>
</div>
<div class="col-md-4">
    <select id="select-city" class="form-control">
        @if(empty($cities))
            <option value="0" disabled selected>Cities is empty</option>
        @elseif(empty($city_id))
            <option value="0" disabled selected>Please select the region</option>
        @endif
        @foreach($cities as $city)
            @if($city->id == $city_id)
                <option value="{{ $city->id }}" selected>{{ $city->name }}</option>
            @else
                <option value="{{ $city->id }}">{{ $city->name }}</option>
            @endif
        @endforeach
    </select>
</div>
