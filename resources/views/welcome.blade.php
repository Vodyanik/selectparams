<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Test</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div id="select">
                    <div class="col-md-4">
                        <select id="select-country" class="form-control">
                            @if(empty($countries))
                                <option value="0" disabled selected>Countries is empty</option>
                            @elseif(empty($country_id))
                                <option value="0" disabled selected>Please select the country</option>
                            @endif
                            @foreach($countries as $country)
                                @if($country->id == $country_id)
                                    <option value="{{ $country->id }}" selected>{{ $country->name }}</option>
                                @else
                                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <select id="select-region" class="form-control">
                            @if(empty($regions))
                                <option value="0" disabled selected>Regions is empty</option>
                            @elseif(empty($region_id))
                                <option value="0" disabled selected>Please select the country</option>
                            @endif
                            @foreach($regions as $region)
                                @if($region->id == $region_id)
                                    <option value="{{ $region->id }}" selected>{{ $region->name }}</option>
                                @else
                                    <option value="{{ $region->id }}">{{ $region->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <select id="select-city" class="form-control">
                            @if(empty($cities))
                                <option value="0" disabled selected>Cities is empty</option>
                            @elseif(empty($city_id))
                                <option value="0" disabled selected>Please select the region</option>
                            @endif
                            @foreach($cities as $city)
                                @if($city->id == $city_id)
                                    <option value="{{ $city->id }}" selected>{{ $city->name }}</option>
                                @else
                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>1</th>
                            <th>2</th>
                            <th>3</th>
                            <th>4</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>1</td>
                            <td>2</td>
                            <td>3</td>
                            <td>4</td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>1</td>
                            <td>2</td>
                            <td>3</td>
                            <td>4</td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>1</td>
                            <td>2</td>
                            <td>3</td>
                            <td>4</td>
                        </tr>
                        <tr>
                            <th scope="row">4</th>
                            <td>1</td>
                            <td>2</td>
                            <td>3</td>
                            <td>4</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script>
            $(document).ready(function () {
                var countryId = '';
                var countryParam = '?country_id={{ $country_id  }}';
                var regionParam = '&region_id={{ $region_id }}';
                var cityParam = '&city_id={{ $city_id }}';
                var params = '';

                $("#select").on('change', '#select-country', function () {
                    countryId = $(this).val();
                    countryParam = '?country_id=' + countryId;
                    params = countryParam;
                    history.pushState(null, null, params);
                    ajaxSelectRequest(params);
                });

                $("#select").on('change', '#select-region', function () {
                    let regionId = $(this).val();
                    regionParam = '&region_id=' + regionId;
                    params = countryParam + regionParam;
                    history.pushState(null, null, params);
                    ajaxSelectRequest(params);
                });

                $("#select").on('change', '#select-city', function () {
                    let cityId = $(this).val();
                    cityParam = '&city_id=' + cityId;
                    params = countryParam + regionParam + cityParam;
                    history.pushState(null, null, params);
                    ajaxSelectRequest(params);
                });

                function ajaxSelectRequest(param) {
                    $.ajax({
                        url: '<?php route('index') ?>',
                        params: param,
                        success: function (html) {
                            $('#select').empty().append(html);
                        }
                    });
                }
            });
        </script>
    </body>
</html>
