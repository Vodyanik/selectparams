<?php

namespace App\Http\Controllers;

use App\Country;
use App\Region;
use App\City;
use Illuminate\Http\Request;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $country_id = 0;
        $region_id = 0;
        $city_id = 0;

        $regions = $cities = [];

        $countries = Country::all();
        if(!empty($request->input('country_id'))) {
            $country_id = $request->input('country_id');
            $regions = Region::where(['country_id' => $country_id])->get();

            if(!empty($request->input('region_id'))) {
                $region_id = $request->input('region_id');
                $cities = City::where(['region_id' => $region_id])->get();

                if(!empty($request->input('city_id'))) {
                    $city_id = $request->input('city_id');
                }
            }
        }

        if ($request->ajax()) {
            return response()->view('elements', [
                'countries' => $countries,
                'regions' => $regions,
                'cities' => $cities,
                'country_id' => $country_id,
                'region_id' => $region_id,
                'city_id' => $city_id,
            ]);
        }

        return view('welcome', [
            'countries' => $countries,
            'regions' => $regions,
            'cities' => $cities,
            'country_id' => $country_id,
            'region_id' => $region_id,
            'city_id' => $city_id,
        ]);
    }
}
